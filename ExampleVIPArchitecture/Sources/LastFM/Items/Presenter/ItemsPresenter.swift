//
//  ItemsPresenter.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

// MARK: - ItemsPresenterInput

/// _ItemsPresenterInput_ is a protocol for presenter input behaviours
protocol ItemsPresenterInput: ItemsInteractorOutput {
    
}

// MARK: - ItemsPresenterOutput

/// _ItemsPresenterOutput_ is a protocol for presenter output behaviours
protocol ItemsPresenterOutput: class {
    
    /// Tells the output to display artists
    ///
    /// - parameter viewModels: The artist view models
    func displayItems(viewModels: [ItemViewModel])
    
    /// Tells the output to dislpay an error
    ///
    /// - parameter viewModel: The error view model
    func displayError(viewModel: ErrorViewModel)
}

// MARK: - ArtistsPresenter

/// _ArtistsPresenter_ is a class responsible for presenting artists logic
final class ItemsPresenter {
    
    private(set) weak var output: ItemsPresenterOutput?
    
    // MARK: - Initializers
    
    init(output: ItemsPresenterOutput) {
        
        self.output = output
    }
}

// MARK: - ItemsPresenterInput

extension ItemsPresenter: ItemsPresenterInput {
    
    /// Prepares the item models for presentation and tells the output to display artists
    ///
    /// - parameter artists: The list of artists
    func presentItems(items: [Artist]) {
        
        let viewModels = items.compactMap { artist -> ItemViewModel in
            var imageURL = URL(string: "")
            if let imageArray = artist.image {
                let imageURLs = imageArray.compactMap { (imageDictionary) -> URL? in                    
                    if let imageSize = imageDictionary.size, imageSize == ArtistsConstants.imageSizeValue,
                        let imageURLString = imageDictionary.text {
                        return URL(string: imageURLString)
                    } else if  let imageURLString = imageDictionary.text {
                        return URL(string: imageURLString)
                    }
                    
                    return nil
                }
                imageURL = imageURLs.last
            }

            return ItemViewModel(title: artist.name ?? "", imageURL: imageURL)
        }
        
        output?.displayItems(viewModels: viewModels)
    }
    
    /// Prepares the error model for presentation and tells the output to display error
    ///
    /// - parameter error: The error
    func presentError(error: Error) {
        
        let errorViewModel = ErrorViewModel(title: Strings.Error.genericTitle,
                                            message: Strings.Error.genericMessage,
                                            buttonTitles: [Strings.Error.okButtonTitle])
        
        output?.displayError(viewModel: errorViewModel)
    }
}
  
