//
//  ItemsViewController.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

private struct Constants {
    
    static let rowHeight: CGFloat = 160.0
}

// MARK: - ItemsViewControllerInput

/// _ItemsViewControllerInput_ is a protocol for view controller input behaviours
protocol ItemsViewControllerInput: ItemsPresenterOutput {
    
}

// MARK: - ItemsViewControllerOutput

/// _ItemsViewControllerInput_ is a protocol for view controller output behaviours
protocol ItemsViewControllerOutput {
    
    var items: [Artist]? { get }
    
    /// Tells the output (interactor) to fetch top items
    func fetchItems()
}

// MARK: - ItemsViewController

/// _ItemsViewController_ is a view controller responsible for displaying a list of top artists
final class ItemsViewController: UIViewController, ErrorPresenter {
    
    @IBOutlet weak var itemsTableView: UITableView!
    var output: ItemsViewControllerOutput!
  //  var router: ItemsRouterProtocol!
    
    let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
    let refreshControl = UIRefreshControl()

    fileprivate var itemsViewModels: [ItemViewModel] = []
    
    // MARK: - Initializers
    
    /// Initializes an instance of _ItemsViewController_ with a configurator
    ///
    /// - parameter configurator: The configurator
    ///
    /// - returns: The instance of _ItemsViewController_
//    init(configurator: ItemsConfigurator = ItemsConfigurator.sharedInstance) {
//        
//        super.init(nibName: nil, bundle: nil)
//        
//        configure(configurator: configurator)
//    }
    
    /// Initializes an instance of _ItemsViewController_ from storyboard
    ///
    /// - parameter coder: The coder
    ///
    /// - returns: The instance of _ItemsViewController_
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        configure()
    }
    
    // MARK: - Configurator
    
    private func configure(configurator: ItemsConfigurator = ItemsConfigurator.sharedInstance) {
        
        configurator.configure(viewController: self)
    }

    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupTitle()
        setupTableView()
        fetchItems()
    }
    
    // MARK: - Setup
    
    private func setupTitle() {
        
        title = Strings.Artists.screenTitle
    }
    
    private func setupTableView() {
        itemsTableView.rowHeight = Constants.rowHeight
        itemsTableView.separatorStyle = .singleLine
        itemsTableView.separatorColor = UIColor.white
        itemsTableView.separatorInset = UIEdgeInsets.zero
        itemsTableView.layoutMargins = UIEdgeInsets.zero
        itemsTableView.showsVerticalScrollIndicator = false
        itemsTableView.insertSubview(refreshControl, at: 0)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    // MARK: - Event handling
    
    /// Asks the output to fetch items
    func fetchItems() {
        showActivityIndicator(viewController: self, activityIndicator: activityIndicator)

        output.fetchItems()
    }
    
    /// Asks the output to fetch items. Called when there is a need to refresh the items list
    @objc func refresh() {
        
        fetchItems()
    }
}

// MARK: - UITableViewDataSource

extension ItemsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return itemsViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell: ItemTableViewCell! = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.reuseIdentifier()) as? ItemTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: ItemTableViewCell.reuseIdentifier(), bundle: nil), forCellReuseIdentifier: ItemTableViewCell.reuseIdentifier())
             cell = tableView.dequeueReusableCell(withIdentifier: ItemTableViewCell.reuseIdentifier()) as? ItemTableViewCell
        }
        let viewModel = itemsViewModels[indexPath.row]
        cell.viewModel = viewModel
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ItemsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //router.navigateToItem(atIndexPath: indexPath, animated: true)
        self.performSegue(withIdentifier: "ItemsViewControllerToItemsDetailViewController", sender: self)
    }
}

// MARK: - ItemsPresenterOutput

extension ItemsViewController: ItemsViewControllerInput {
    
    func displayItems(viewModels: [ItemViewModel]) {
        hideActivityIndicator(viewController: self, activityIndicator: activityIndicator)
        itemsViewModels = viewModels
        itemsTableView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    func displayError(viewModel: ErrorViewModel) {
        hideActivityIndicator(viewController: self, activityIndicator: activityIndicator)
        self.refreshControl.endRefreshing()
        presentError(viewModel: viewModel)
    }
}
