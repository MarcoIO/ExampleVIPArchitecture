//
//  ItemsViewModel.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Foundation

// MARK: - ItemsViewModel

/// _ItemsViewModel_ is a model representing an artist with title and image
struct ItemViewModel {
    
    let title: String
    let imageURL: URL?
}
