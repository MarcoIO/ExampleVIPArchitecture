//
//  ItemsViewController+ItemsRouter.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 26/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Foundation
import UIKit
extension ItemsViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ItemsViewControllerToItemsDetailViewController" {
            if let items = self.output.items, let indexPath = self.itemsTableView.indexPathForSelectedRow, indexPath.row < items.count {
                let selectedItem = items[indexPath.row]
                guard let vcontroller: ItemsDetailViewController = (segue.destination as? ItemsDetailViewController) else {
                    return
                }
                vcontroller.item = selectedItem
            }
        }
    }
}
