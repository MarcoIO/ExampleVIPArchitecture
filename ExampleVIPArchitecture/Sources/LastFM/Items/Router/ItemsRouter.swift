//
//  ItemsRouter.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

// MARK: - ItemsRouterProtocol

/// _ItemsRouterProtocol_ is a protocol for router input behaviours
protocol ItemsRouterProtocol {
    
    var viewController: ItemsViewController? { get }
    
    /// Handles the navigation when selecting an artist in the list
    ///
    /// - parameter indexPath: The selected index path
    /// - parameter animated: Flag indicating whether the updates should be animate or not
    func navigateToItem(atIndexPath indexPath: IndexPath, animated: Bool)
}

// MARK: - ItemsRouter

/// _ItemsRouter_ is a class responsible for routing from _ItemsViewController_
final class ItemsRouter {
    
    weak var viewController: ItemsViewController?
    
    // MARK: - Initializers
    
    init(viewController: ItemsViewController) {
        
        self.viewController = viewController
    }
}

// MARK: - ArtistsRouterProtocol

extension ItemsRouter: ItemsRouterProtocol {
    
    /// Handles the navigation when selecting an artist in the list to artist detail
    ///
    /// - parameter indexPath: The selected index path
    /// - parameter animated: Flag indicating whether the updates should be animate or not
    func navigateToItem(atIndexPath indexPath: IndexPath, animated: Bool = false) {
        
        if let items = viewController?.output.items, indexPath.row < items.count {
            
            _ = items[indexPath.row]
            
          //  let itemViewController = ItemsViewController(item: selectedItem)
            //viewController?.navigationController?.pushViewController(itemViewController, animated: animated)
        }
    }
}
