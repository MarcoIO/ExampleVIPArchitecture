//
//  ItemsConfigurator.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

// MARK: - ItemsConfigurator

/// _ItemsConfigurator_ is a class responsible for configuring the VIP scene pathways for _ItemsViewController_
final class ItemsConfigurator {
    
    /// Singleton instance of ItemsConfigurator
    static let sharedInstance = ItemsConfigurator()
    
    // MARK: - Configuration
    
    /// Configures the VIP scene with pathways between router, view controller, interactor and presenter
    ///
    /// - parameter viewController: The view controller
    func configure(viewController: ItemsViewController) {
        
       // let router = ItemsRouter(viewController: viewController)
        let presenter = ItemsPresenter(output: viewController)
        let interactor = ItemsInteractor(output: presenter)
        
        viewController.output = interactor
       // viewController.router = router
    }
}
