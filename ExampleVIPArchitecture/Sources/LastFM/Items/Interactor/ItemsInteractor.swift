//
//  ItemsInteractor.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

// MARK: - ItemsInteractorInput

/// _ItemsInteractorInput_ is a protocol for interactor input behaviours
protocol ItemsInteractorInput: ItemsViewControllerOutput {
    
}

// MARK: - ItemsInteractorOutput

/// _ItemsInteractorOutput_ is a protocol for interactor output behaviours
protocol ItemsInteractorOutput {
    
    /// Tells the output to present items
    ///
    /// - parameter items: The list of items to present
    func presentItems(items: [Artist])
    
    /// Tells the output to present an error
    ///
    /// - parameter error: The error to present
    func presentError(error: Error)
}

// MARK: - ItemsInteractor

/// _ItemsInteractor_ is an interactor responsible for top items business logic
final class ItemsInteractor {
    
    let output: ItemsInteractorOutput
    let worker: ItemsWorker
    
    var items: [Artist]?
    
    // MARK: - Initializers
    
    /// Initializes an instance of _ItemsInteractor_ with an output and a worker
    ///
    /// - parameter output: The interactors output
    /// - parameter worker: The artits worker used to fetch items
    ///
    /// - returns: An instance of _ItemsInteractor_
    init(output: ItemsInteractorOutput, worker: ItemsWorker = ItemsWorker()) {
        
        self.output = output
        self.worker = worker
    }
}

extension ItemsInteractor: ItemsInteractorInput {
    
    /// Fetches a list of top items through the worker
    func fetchItems() {
        worker.fetchItems { (result) in
            if let error = result.error {
                print(error)
                self.output.presentError(error: error)

            }
            if let value = result.value {
                print(value)
                if let list = value.artists?.artist {
                    self.items = list
                    self.output.presentItems(items: list)
                }
                
            }
            
        }
    }
}
