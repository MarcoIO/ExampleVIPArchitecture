//
//  ItemsAPIStore.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Foundation

// MARK: - ItemsAPIStore

/// _ItemsAPIStore_ is a class responsible for fetching artists
final class ItemsAPIStore {
    
    fileprivate struct Constants {
        static let topItemsLimit = 50
        static let topItemsDictionaryKey = "artists"
        static let topItemsArrayKey = "artist"
    }
    
}

// MARK: - ArtistsStoreProtocol

extension ItemsAPIStore: ItemsStoreProtocol {
    /// Fetches a list of top artists
    ///
    /// - parameter completion: The completion block
    func fetchItems(completion: @escaping (Results<Artists>) -> Void) -> RequestToken {
        let limit = Constants.topItemsLimit
        let webResource = WebResource<Artists>(urlPath: .getTopItems(limit), method: .get, header: nil, decode: (Artists.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
        }
    }
