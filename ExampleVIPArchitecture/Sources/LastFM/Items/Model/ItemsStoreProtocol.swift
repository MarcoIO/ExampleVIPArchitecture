//
//  ItemsStoreProtocol.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Foundation

// MARK: - ItemsStoreError

/// _ItemsStoreError_ is an enumeration for artist store errors
///
/// - generic:         Generic error
/// - invalidURL:      Invalid URL error
/// - invalidResponse: Invalid response
enum ItemsStoreError: Error {
    
    case generic
    case invalidURL
    case invalidResponse
}

// MARK: - ArtistsStoreProtocol

/// _ArtistsStoreProtocol_ is a protocol  for artist store behaviors
protocol ItemsStoreProtocol {
    
    /// Fetches artists from a store (API, memory, etc)
    ///
    /// - parameter completion: The completion block
    func fetchItems(completion: @escaping (Results<Artists>) -> Void) -> RequestToken
}
