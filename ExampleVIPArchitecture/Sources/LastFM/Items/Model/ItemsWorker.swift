//
//  ItemsWorker.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

// MARK: - ItemsWorker

/// _ItemsWorker_ is a worker object responsible to fetch items from a store
class ItemsWorker {
    
    fileprivate var store: ItemsStoreProtocol
    
    // MARK: - Initializers
    
    /// Initializes an _ItemsWorker_ with a store
    ///
    /// - parameter store: A store where to fetch items from (API, memory, etc)
    ///
    /// - returns: The instance of _ItemsWorker_
    init(store: ItemsStoreProtocol = ItemsAPIStore()) {
        
        self.store = store
    }
    
    // MARK: - Business Logic
    
    /// Fetches items from a store
    ///
    /// - parameter completion: The completion block
    func fetchItems(completion: @escaping (Results<Artists>) -> Void) {
        
        _ = store.fetchItems(completion: completion)
    }
}
