//
//  ItemsDetailRouter.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 23/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

protocol ItemsDetailRouterProtocol {

    var viewController: ItemsDetailViewController? { get }

    func navigateToSomewhere()
}

final class ItemsDetailRouter {

    weak var viewController: ItemsDetailViewController?

    // MARK: - Initializers

    init(viewController: ItemsDetailViewController?) {

        self.viewController = viewController
    }
}

// MARK: - ItemsDetailRouterProtocol

extension ItemsDetailRouter: ItemsDetailRouterProtocol {

    // MARK: - Navigation

    func navigateToSomewhere() {

    }
}
