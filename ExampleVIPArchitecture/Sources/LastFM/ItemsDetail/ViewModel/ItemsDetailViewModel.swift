//
//  ItemsDetailViewModel.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 23/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

struct ItemsDetailViewModel {
    let title: String
    let description: String
    let imageURL: URL?
    let rank: String?
}
