//
//  ItemsDetailConfigurator.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 23/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

final class ItemsDetailConfigurator {

    // MARK: - Singleton

    static let sharedInstance: ItemsDetailConfigurator = ItemsDetailConfigurator()

    // MARK: - Configuration

    func configure(viewController: ItemsDetailViewController) {

      //  let router = ItemsDetailRouter(viewController: viewController)
        let presenter = ItemsDetailPresenter(output: viewController)
        let interactor = ItemsDetailInteractor(output: presenter)

        viewController.output = interactor
        //viewController.router = router
    }
}
