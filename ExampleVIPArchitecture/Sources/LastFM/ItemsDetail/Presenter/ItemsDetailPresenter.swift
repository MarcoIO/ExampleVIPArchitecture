//
//  ItemsDetailPresenter.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 23/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

protocol ItemsDetailPresenterInput: ItemsDetailInteractorOutput {

}

protocol ItemsDetailPresenterOutput: class {

    /// Tells the output to display albums
    ///
    /// - parameter viewModels: The album view models
    func displayAlbums(viewModels: [ItemsDetailViewModel])
    
    /// Tells the output to dislpay an error
    ///
    /// - parameter viewModel: The error view model
    func displayError(viewModel: ErrorViewModel)
    
}

final class ItemsDetailPresenter {

    private(set) weak var output: ItemsDetailPresenterOutput!

    // MARK: - Initializers

    init(output: ItemsDetailPresenterOutput) {

        self.output = output
    }
}

// MARK: - ItemsDetailPresenterInput

extension ItemsDetailPresenter: ItemsDetailPresenterInput {
    // MARK: - Presentation logic

    func presentAlbums(albums: [Album]) {
        let viewModels = albums.compactMap { album -> ItemsDetailViewModel in
            var imageURL = URL(string: "")
            if let imageArray = album.image {
                let imageURLs = imageArray.compactMap { (imageDictionary) -> URL? in
                    if let imageSize = imageDictionary.size, imageSize == ArtistsConstants.imageSizeValue,
                        let imageURLString = imageDictionary.text {
                        return URL(string: imageURLString)
                    } else if  let imageURLString = imageDictionary.text {
                        return URL(string: imageURLString)
                    }
                    
                    return nil
                }
                imageURL = imageURLs.last
            }
            return ItemsDetailViewModel(title: album.name ?? "Name", description: album.listeners ?? "0", imageURL: imageURL, rank: album.rank)
        }
        
        output?.displayAlbums(viewModels: viewModels)
    }
    
    func presentError(error: Error) {
        let errorViewModel = ErrorViewModel(title: Strings.Error.genericTitle,
                                            message: Strings.Error.genericMessage,
                                            buttonTitles: [Strings.Error.okButtonTitle])
        
        output?.displayError(viewModel: errorViewModel)
    }

}
