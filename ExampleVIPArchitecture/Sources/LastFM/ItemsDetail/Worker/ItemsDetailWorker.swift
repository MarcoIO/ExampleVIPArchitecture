//
//  ItemsDetailWorker.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 23/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

class ItemsDetailWorker {

    fileprivate var store: ItemsDetailStoreProtocol
    
    // MARK: - Initializers
    
    /// Initializes an ItemsDetailWorker with a store
    ///
    /// - parameter store: A store where to fetch items from (API, memory, etc)
    ///
    /// - returns: The instance of ItemsDetailWorker
    init(store: ItemsDetailStoreProtocol = ItemsDetailAPIStore()) {
        
        self.store = store
    }
    
    // MARK: - Business Logic
    
    /// Fetches items from a store
    ///
    /// - parameter completion: The completion block
    func fetchAlbums(artistId: String, completion: @escaping (Results<Albums>) -> Void) {
        _ = store.fetchAlbums(artistId: artistId, completion: completion)
    }
}
