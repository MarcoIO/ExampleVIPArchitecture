//
//  ItemsDetailAPIStore.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 26/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

// MARK: - ItemsDetailAPIStore

import Foundation

// MARK: - ItemsDetailAPIStore

/// ItemsDetailAPIStore is a class responsible for fetching albums
final class ItemsDetailAPIStore {
    
    fileprivate struct Constants {
        static let topAlbumsLimit = 50
        static let topAlbumsDictionaryKey = "topalbums"
        static let topAlbumsArrayKey = "album"
    }
    
}

// MARK: - ArtistsStoreProtocol

extension ItemsDetailAPIStore: ItemsDetailStoreProtocol {
    /// Fetches a list of top albums artist
    ///
    /// - parameter completion: The completion block
    func fetchAlbums(artistId: String, completion: @escaping (Results<Albums>) -> Void) -> RequestToken {
        let limit = Constants.topAlbumsLimit
        let webResource = WebResource<Albums>(urlPath: .getTopAlbums(artistId, limit), method: .get, header: nil, decode: (Albums.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
}
