//
//  ItemsDetailTableViewCell.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 26/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit
import Kingfisher
// MARK: - ItemTableViewCell

/// ItemsDetailTableViewCell is the table view cell responsible to display an item
class ItemsDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageOverlayView: UIView!
    @IBOutlet weak var imageItem: UIImageView!
    var viewModel: ItemsDetailViewModel? {
        
        didSet {
            rankLabel.text = viewModel?.rank
            titleLabel.text = viewModel?.title
            imageItem.kf.setImage(with: viewModel?.imageURL)
            imageOverlayView.backgroundColor = UIColor.darkGray
            imageOverlayView.alpha = 0.7
        }
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        //  super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: - Reuse Identifier
    
    open class func reuseIdentifier() -> String {
        
        return "ItemsDetailTableViewCell"
    }
    
}
