//
//  ItemsDetailViewController.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 23/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit
private struct Constants {
    
    static let rowHeight: CGFloat = 160.0
}
protocol ItemsDetailViewControllerInput: ItemsDetailPresenterOutput {

}

protocol ItemsDetailViewControllerOutput {

    var albums: [Album]? { get }
    
    /// Tells the output (interactor) to fetch albums for artist
    ///
    /// - parameter artistId: The artist identifier
    func fetchAlbums(artistId: String)
    
}

final class ItemsDetailViewController: UIViewController, ErrorPresenter {

    var output: ItemsDetailViewControllerOutput!
    var item: Artist?
    @IBOutlet weak var itemsTableView: UITableView!
    
    let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
    let refreshControl = UIRefreshControl()
    fileprivate var itemsDetailViewModels: [ItemsDetailViewModel] = []

    // MARK: - Initializers
    /// Initializes an instance of _ItemsViewController_ from storyboard
    ///
    /// - parameter coder: The coder
    ///
    /// - returns: The instance of _ItemsViewController_
    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

        configure()
    }

    // MARK: - Configurator

    private func configure(configurator: ItemsDetailConfigurator = ItemsDetailConfigurator.sharedInstance) {

        configurator.configure(viewController: self)
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {

        super.viewDidLoad()
        setupTitle()
        setupTableView()
        fetchAlbums()
    }

    // MARK: - Setup
    
    private func setupTitle() {
      
        title = Strings.Albums.screenTitle
          if let artistName = item?.name, let str = title {
            title = str + "-" + artistName
        }
    }
    
    private func setupTableView() {
        itemsTableView.rowHeight = Constants.rowHeight
        itemsTableView.separatorStyle = .singleLine
        itemsTableView.separatorColor = UIColor.white
        itemsTableView.separatorInset = UIEdgeInsets.zero
        itemsTableView.layoutMargins = UIEdgeInsets.zero
        itemsTableView.showsVerticalScrollIndicator = false
        itemsTableView.insertSubview(refreshControl, at: 0)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    // MARK: - Event handling
    
    /// Asks the output to fetch albums
    func fetchAlbums() {
        
        if let artistId = item?.mbid {
            showActivityIndicator(viewController: self, activityIndicator: activityIndicator)
            output.fetchAlbums(artistId: artistId)
        }
    }
    
    /// Asks the output to fetch albums. Called when there is a need to refresh the album list
    @objc func refresh() {
        
        fetchAlbums()
    }
}
// MARK: - UITableViewDataSource

extension ItemsDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return itemsDetailViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ItemsDetailTableViewCell! = tableView.dequeueReusableCell(withIdentifier: ItemsDetailTableViewCell.reuseIdentifier()) as? ItemsDetailTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: ItemsDetailTableViewCell.reuseIdentifier(), bundle: nil), forCellReuseIdentifier: ItemsDetailTableViewCell.reuseIdentifier())
            cell = tableView.dequeueReusableCell(withIdentifier: ItemsDetailTableViewCell.reuseIdentifier()) as? ItemsDetailTableViewCell
        }
        let viewModel = itemsDetailViewModels[indexPath.row]
        
        cell.viewModel = viewModel
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ItemsDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

// MARK: - ItemsDetailPresenterOutput

extension ItemsDetailViewController: ItemsDetailViewControllerInput {
    func displayAlbums(viewModels: [ItemsDetailViewModel]) {
        hideActivityIndicator(viewController: self, activityIndicator: activityIndicator)
        itemsDetailViewModels = viewModels
        self.itemsTableView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    func displayError(viewModel: ErrorViewModel) {
        hideActivityIndicator(viewController: self, activityIndicator: activityIndicator)
        self.refreshControl.endRefreshing()
        presentError(viewModel: viewModel)
    }
}
