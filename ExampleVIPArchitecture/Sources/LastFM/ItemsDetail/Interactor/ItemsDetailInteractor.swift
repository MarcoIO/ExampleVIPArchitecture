//
//  ItemsDetailInteractor.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 23/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

protocol ItemsDetailInteractorInput: ItemsDetailViewControllerOutput {

}

protocol ItemsDetailInteractorOutput {

    /// Tells the output to present albums
    ///
    /// - parameter albums: The list of albums to present
    func presentAlbums(albums: [Album])
    
    /// Tells the output to present an error
    ///
    /// - parameter error: The error to present
    func presentError(error: Error)
}

final class ItemsDetailInteractor {

    let output: ItemsDetailInteractorOutput
    let worker: ItemsDetailWorker
    var albums: [Album]?

    // MARK: - Initializers

    init(output: ItemsDetailInteractorOutput, worker: ItemsDetailWorker = ItemsDetailWorker()) {

        self.output = output
        self.worker = worker
    }
}

// MARK: - ItemsDetailInteractorInput

extension ItemsDetailInteractor: ItemsDetailViewControllerOutput {
    
    // MARK: - Business logic
    
    /// Fetches a list of top albums for an artist through the worker
    ///
    /// - parameter artistId: The artist identifier
    func fetchAlbums(artistId: String) {
        worker.fetchAlbums(artistId: artistId) { (result) in
            
            if let error = result.error {
                print(error)
                self.output.presentError(error: error)
                
            }
            if let value = result.value {
                print(value)
                self.albums = value.topalbums?.album
                if let albumsArray = value.topalbums?.album {
                    self.output.presentAlbums(albums: albumsArray)
                }
            }
        }
    }
}
