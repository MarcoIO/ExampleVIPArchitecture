//
//  ErrorViewModel.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Foundation

/// _ErrorViewModel_ is a view model containing info related to displaying an error
struct ErrorViewModel {
    
    let title: String?
    let message: String?
    let buttonTitles: [String]?
}
