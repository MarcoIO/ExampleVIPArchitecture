//
//  Strings.swift
//  BaseVIPArchitectureProject
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Foundation

/// _Strings_ is a struct responsible to hold all the strings in this application
struct Strings {
    
    struct Artists {
        
        static let screenTitle = NSLocalizedString("Top Artists", comment: "Top Artists screen title")
    }
    
    struct ListCharactersRickAndMorty {
        
        static let screenTitle = NSLocalizedString("Characters", comment: "")
    }
    struct Albums {
        
        static let screenTitle = NSLocalizedString("Top Albums", comment: "Artist header title")
    }
    
    struct Error {
        
        static let genericTitle = NSLocalizedString("Sorry", comment: "Generic error title")
        static let genericMessage = NSLocalizedString("Something went wrong.", comment: "Generic error message")
        static let okButtonTitle = NSLocalizedString("Ok", comment: "Alert button title")
    }
}
