//
//  WebSource.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//
import UIKit
import Alamofire
struct WebResource<A> {
    var urlPath: URLPath
    var method: HTTPMethod = .get
    var header: [String: String]?
    var decode: (Data) -> Results<A>
    
    func request(completion: @escaping (Results<A>) -> Void) -> RequestToken {
        return WebResourceManager.shared.fetchRequest(resource: self, completion: completion)
    }
}

extension Decodable {
    static func decode<T: Codable>(_ data: Data) -> Results<T> {
        if let decoded = try? JSONDecoder().decode(T.self, from: data) {
            return .value(decoded)
        }
        return .error(.missingData)
    }
}
