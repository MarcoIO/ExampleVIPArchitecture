//
//  WebResourceManager.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Alamofire

class WebResourceManager {
    
    static let shared = WebResourceManager()

    //////
    func fetchRequest<A>(resource: WebResource<A>, completion: @escaping (Results<A>) -> Void) -> RequestToken {
        let url = resource.urlPath.url!
        let parameter = resource.method.parameter
        var httpHeaders: HTTPHeaders?
        if let header = resource.header {
            httpHeaders = HTTPHeaders.init(header)
        }
        print(url)
        let dataRequest = AF.request(url, method: resource.method.requestMethod, parameters: parameter, encoding: JSONEncoding.default, headers: httpHeaders).responseData { (response) in
            
            if let code = response.response?.statusCode {
                if 200...299 ~= code {
                    print("All well")
                }
                if 300...399 ~= code {
                    print("Huhh")
                }
                if 400...499 ~= code {
                    let nserror = NSError(domain: "com.errorbase", code: code, userInfo: nil)
                    completion(.error(.serverError(nserror)))
                    return
                }
            }
            
            switch response.result {
            case .success (let value):
                completion(resource.decode(value))
                return
            case .failure (let error):
                print(error)
                completion(.error(.serverError(error)))
                return
            }
            
        }
        
        let task = dataRequest.task
        
        return RequestToken(task: task)
    }
}

class RequestToken {
    private weak var task: URLSessionTask?
    
    init(task: URLSessionTask?) {
        self.task = task
    }
    
    func cancel() {
        self.task?.cancel()
    }
}
