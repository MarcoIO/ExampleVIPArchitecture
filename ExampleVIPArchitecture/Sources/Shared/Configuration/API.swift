//
//  API.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//
import Foundation
import Alamofire

enum URLScheme: String {
    case http
    case https
}
enum URLHost: String {
    case live = "https://ws.audioscrobbler.com/2.0/"
    case liveRickAndMorty = "https://rickandmortyapi.com/api/character/"
    
    var host: String {
        return self.rawValue
    }
    
    var fixedPath: String {
        return ""
    }
    static let baseURLString = "https://ws.audioscrobbler.com/2.0/"
    static let apiKey = "bc1135dd25cef79a188dbbf985d5c850"
}
enum URLPath {
    case getTopItems(Int)
    case getTopAlbums(String, Int)
    case getAllCharactersRickAndMorty(Int)
    var path: String {
        switch self {
            
        case .getTopItems(let limit):
            
            let method = "chart.gettopartists"
            
            return "?method=\(method)&api_key=\(URLHost.apiKey)&format=json&limit=\(limit)"
            
        case .getTopAlbums(let artistId, let limit):
            
            let method = "artist.gettopalbums"
            
            return "?method=\(method)&api_key=\(URLHost.apiKey)&mbid=\(artistId)&format=json&limit=\(limit)"
        case .getAllCharactersRickAndMorty(let page):
            return "?page=\(page)"
        }
        
    }
    var url: URL? {
        switch self {
        case .getTopItems, .getTopAlbums:
            AppConfig.host = .live
             return URL(string: AppConfig.host.rawValue + self.path)
        case .getAllCharactersRickAndMorty:
            AppConfig.host = .liveRickAndMorty
             return URL(string: AppConfig.host.rawValue + self.path)
        }
    }
}
struct AppConfig {
    static let scheme: URLScheme = .https
    static var host: URLHost = .live
}
typealias JSONType = [String: Any]

enum HTTPMethod {
    case get
    case post(JSONType?)
    case put(JSONType?)
    
    var requestMethod: Alamofire.HTTPMethod {
        switch self {
        case .get:
            return Alamofire.HTTPMethod.get
        case .post:
            return Alamofire.HTTPMethod.post
        case .put:
            return Alamofire.HTTPMethod.put
        }
    }
    
    var parameter: JSONType? {
        switch self {
        case let .post(parameter):
            return parameter
        case let .put(parameter):
            return parameter
        default:
            return nil
        }
    }
}
enum AppError: Error {
    case missingData
    case serverError(Error)
    case sessionExpired
    case notReachable
}
enum Results<A> {
    case value(A)
    case error(AppError)
    
    var error: AppError? {
        switch self {
        case .error(let b):
            return b
        default:
            return nil
        }
    }
    
    var value: A? {
        switch self {
        case .value(let a):
            return a
        default:
            return nil
        }
    }
}
