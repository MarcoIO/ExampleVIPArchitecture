//
//  UIViewController+Preview.swift
//  ExampleVIPArchitecture
//
//  Created by marco.iniguez.ollero on 14/10/2020.
//  Copyright © 2020 Iniguez Ollero, Marco. All rights reserved.
//


import UIKit

#if DEBUG
import SwiftUI

@available(iOS 13, *)
extension UIViewController {
    private struct Preview: UIViewControllerRepresentable {
        // this variable is used for injecting the current view controller
        let viewController: UIViewController

        func makeUIViewController(context: Context) -> UIViewController {
            return viewController
        }

        func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        }
    }

    func toPreview() -> some View {
        // inject self (the current view controller) for the preview
        Preview(viewController: self)
    }
}
#endif
