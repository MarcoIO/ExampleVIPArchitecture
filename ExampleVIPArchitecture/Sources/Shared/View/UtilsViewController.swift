//
//  UtilsViewController.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 14/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Foundation
import UIKit
func showActivityIndicator(viewController: UIViewController, activityIndicator: UIActivityIndicatorView) {
    activityIndicator.center = CGPoint(x: viewController.view.bounds.size.width/2, y: viewController.view.bounds.size.height/2)
    activityIndicator.color = UIColor.blue
    viewController.view.addSubview(activityIndicator)
    activityIndicator.startAnimating()
}
func hideActivityIndicator(viewController: UIViewController, activityIndicator: UIActivityIndicatorView) {
    activityIndicator.stopAnimating()
}
func showAlert( _ viewController: UIViewController, title: String, subtitle: String) {
    let alert = UIAlertController(title: title, message: subtitle, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
    viewController.present(alert, animated: true, completion: nil)
}
