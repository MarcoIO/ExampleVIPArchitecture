//
//  ListCharacterRouter.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

protocol ListCharacterRouterProtocol {

    var viewController: ListCharacterViewController? { get }

    func navigateToSomewhere()
}

final class ListCharacterRouter {

    weak var viewController: ListCharacterViewController?

    // MARK: - Initializers

    init(viewController: ListCharacterViewController?) {

        self.viewController = viewController
    }
}

// MARK: - ListCharacterRouterProtocol

extension ListCharacterRouter: ListCharacterRouterProtocol {

    // MARK: - Navigation

    func navigateToSomewhere() {

    }
}
