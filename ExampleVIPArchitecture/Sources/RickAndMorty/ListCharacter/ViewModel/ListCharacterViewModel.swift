//
//  ListCharacterViewModel.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

struct ListCharacterViewModel {
    let name: String
    let imageURL: URL?
    let status: String?
    let species: String?
}
