//
//  ListCharacterConfigurator.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

final class ListCharacterConfigurator {

    // MARK: - Singleton

    static let sharedInstance: ListCharacterConfigurator = ListCharacterConfigurator()

    // MARK: - Configuration

    func configure(viewController: ListCharacterViewController) {

        let presenter = ListCharacterPresenter(output: viewController)
        let interactor = ListCharacterInteractor(output: presenter)

        viewController.output = interactor
    }
}
