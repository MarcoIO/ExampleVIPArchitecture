//
//  ListCharacterInteractor.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

protocol ListCharacterInteractorInput: ListCharacterViewControllerOutput {

}

protocol ListCharacterInteractorOutput {

    /// Tells the output to present albums
    ///
    /// - parameter albums: The list of characters to present
    func presentCharacters(characters: [Result])
    
    /// Tells the output to present an error
    ///
    /// - parameter error: The error to present
    func presentError(error: Error)}

final class ListCharacterInteractor {

    let output: ListCharacterInteractorOutput
    let worker: ListCharacterWorker
    var characters: [Result]?
    var page = 0

    // MARK: - Initializers

    init(output: ListCharacterInteractorOutput, worker: ListCharacterWorker = ListCharacterWorker()) {

        self.output = output
        self.worker = worker
    }
}

// MARK: - ListCharacterInteractorInput

extension ListCharacterInteractor: ListCharacterViewControllerOutput {
    // MARK: - Business logic
    
    /// Fetches a list of characters for an artist through the worker
    ///
    /// - parameter artistId: The artist identifier
    func fetchCharacters(reloading: Bool) {
         self.page += 1
        worker.fetchCharacters(page: self.page) { (result) in
            if let error = result.error {
                self.output.presentError(error: error)
                
            }
            if let value = result.value {
                if let chars = self.characters, let results = value.results, reloading {
                    self.characters = chars + results
                } else {
                    self.characters = value.results
                }
                if let charactersArray = self.characters {
                    self.output.presentCharacters(characters: charactersArray)
                }
            }
        }
        
    }
}
