//
//  ListCharactersTableViewCell.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit
import Kingfisher
// MARK: - ItemTableViewCell

/// ItemsDetailTableViewCell is the table view cell responsible to display an item
class ListCharactersTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var speciesLabel: UILabel!
    @IBOutlet weak var imageItem: UIImageView!
    var viewModel: ListCharacterViewModel? {
        
        didSet {
            nameLabel.text = viewModel?.name
            statusLabel.text = viewModel?.status
            imageItem.kf.setImage(with: viewModel?.imageURL)
            speciesLabel.text = viewModel?.species
        }
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        //  super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: - Reuse Identifier
    
    open class func reuseIdentifier() -> String {
        
        return "ListCharactersTableViewCell"
    }
    
}
