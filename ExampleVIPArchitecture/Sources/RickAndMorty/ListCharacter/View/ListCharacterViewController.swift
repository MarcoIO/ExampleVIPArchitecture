//
//  ListCharacterViewController.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit
private struct Constants {
    
    static let rowHeight: CGFloat = 280.0
    static let minItemsToReload: Int = 2

}
protocol ListCharacterViewControllerInput: ListCharacterPresenterOutput {

}

protocol ListCharacterViewControllerOutput {

    var characters: [Result]? { get }
    
    /// Tells the output (interactor) to fetch characters
    ///
    func fetchCharacters(reloading: Bool)
    
}

final class ListCharacterViewController: UIViewController, ErrorPresenter {

    var output: ListCharacterViewControllerOutput!

    @IBOutlet weak var itemsTableView: UITableView!
    
    let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
    let refreshControl = UIRefreshControl()
    var reloadingList: Bool = false
    fileprivate var listCharacterViewModels: [ListCharacterViewModel] = []
    // MARK: - Initializers

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

        configure()
    }

    // MARK: - Configurator

    private func configure(configurator: ListCharacterConfigurator = ListCharacterConfigurator.sharedInstance) {

        configurator.configure(viewController: self)
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {

        super.viewDidLoad()

        setupTitle()
        setupTableView()
        fetchCharacters()
        
    }

    // MARK: - Setup
    
    private func setupTitle() {
        
        title = Strings.ListCharactersRickAndMorty.screenTitle
        
    }
    
    private func setupTableView() {
        itemsTableView.rowHeight = Constants.rowHeight
        itemsTableView.separatorStyle = .singleLine
        itemsTableView.separatorColor = UIColor.white
        itemsTableView.separatorInset = UIEdgeInsets.zero
        itemsTableView.layoutMargins = UIEdgeInsets.zero
        itemsTableView.showsVerticalScrollIndicator = false
        itemsTableView.insertSubview(refreshControl, at: 0)
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    // MARK: - Event handling
    
    /// Asks the output to fetch albums
    func fetchCharacters(reloading: Bool=false) {
        if !reloading {
            showActivityIndicator(viewController: self, activityIndicator: activityIndicator)
        }
        self.reloadingList = true
        output.fetchCharacters(reloading: reloading)
    }
    
    /// Asks the output to fetch albums. Called when there is a need to refresh the album list
    @objc func refresh() {
        
        fetchCharacters()
    }
}

// MARK: - UITableViewDataSource

extension ListCharacterViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listCharacterViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ListCharactersTableViewCell! = tableView.dequeueReusableCell(withIdentifier: ListCharactersTableViewCell.reuseIdentifier()) as? ListCharactersTableViewCell
        if cell == nil {
            tableView.register(UINib(nibName: ListCharactersTableViewCell.reuseIdentifier(), bundle: nil), forCellReuseIdentifier: ListCharactersTableViewCell.reuseIdentifier())
            cell = tableView.dequeueReusableCell(withIdentifier: ListCharactersTableViewCell.reuseIdentifier()) as? ListCharactersTableViewCell
        }
        let viewModel = listCharacterViewModels[indexPath.row]
        
        cell.viewModel = viewModel
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ListCharacterViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == itemsTableView {
            if !listCharacterViewModels.isEmpty {
                if !self.reloadingList &&  listCharacterViewModels.count - (indexPath as NSIndexPath).row < Constants.minItemsToReload {
                    self.reloadingList = true
                        fetchCharacters(reloading: true)
                }
            }
        }
    }
}

// MARK: - ListCharacterPresenterOutput

extension ListCharacterViewController: ListCharacterViewControllerInput {

    // MARK: - Display logic

    func displayCharacters(viewModels: [ListCharacterViewModel]) {
        hideActivityIndicator(viewController: self, activityIndicator: activityIndicator)
        listCharacterViewModels = viewModels
        self.itemsTableView.reloadData()
        self.refreshControl.endRefreshing()
        self.reloadingList = false

    }
    
    func displayError(viewModel: ErrorViewModel) {
        hideActivityIndicator(viewController: self, activityIndicator: activityIndicator)
        self.refreshControl.endRefreshing()
        presentError(viewModel: viewModel)
    }
}
