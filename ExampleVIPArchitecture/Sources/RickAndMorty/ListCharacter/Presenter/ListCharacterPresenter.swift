//
//  ListCharacterPresenter.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

protocol ListCharacterPresenterInput: ListCharacterInteractorOutput {

}

protocol ListCharacterPresenterOutput: class {

    /// Tells the output to display characters
    ///
    /// - parameter viewModels: The characters view models
    func displayCharacters(viewModels: [ListCharacterViewModel])
    
    /// Tells the output to dislpay an error
    ///
    /// - parameter viewModel: The error view model
    func displayError(viewModel: ErrorViewModel)}

final class ListCharacterPresenter {

    private(set) weak var output: ListCharacterPresenterOutput!

    // MARK: - Initializers

    init(output: ListCharacterPresenterOutput) {

        self.output = output
    }
}

// MARK: - ListCharacterPresenterInput

extension ListCharacterPresenter: ListCharacterPresenterInput {

    func presentCharacters(characters: [Result]) {
        let viewModels = characters.compactMap { character -> ListCharacterViewModel in
            var imageURL = URL(string: "")
            if let img = character.image {
                imageURL = URL(string: img)
            }
            return ListCharacterViewModel(name: character.name ?? "", imageURL: imageURL, status: character.status?.rawValue, species: character.species)
        }
        
        output?.displayCharacters(viewModels: viewModels)
    }
    
    func presentError(error: Error) {
        let errorViewModel = ErrorViewModel(title: Strings.Error.genericTitle,
                                            message: Strings.Error.genericMessage,
                                            buttonTitles: [Strings.Error.okButtonTitle])
        
        output?.displayError(viewModel: errorViewModel)
    }
    
}
