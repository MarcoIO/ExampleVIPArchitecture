//
//  ListCharacterWorker.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

class ListCharacterWorker {
    fileprivate var store: ListCharacterStoreProtocol

    // MARK: - Initializers
    
    /// Initializes an ItemsDetailWorker with a store
    ///
    /// - parameter store: A store where to fetch items from (API, memory, etc)
    ///
    /// - returns: The instance of ItemsDetailWorker
    init(store: ListCharacterStoreProtocol = ListCharactersAPIStore()) {
        
        self.store = store
    }
    
    // MARK: - Business Logic
    
    /// Fetches items from a store
    ///
    /// - parameter completion: The completion block
    func fetchCharacters(page: Int, completion: @escaping (Results<CharactersRickAndMorty>) -> Void) {
        _ = store.fetchCharacters(page: page, completion: completion)
    }
}
