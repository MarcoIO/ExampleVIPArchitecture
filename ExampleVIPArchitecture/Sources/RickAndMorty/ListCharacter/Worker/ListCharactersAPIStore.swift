//
//  ListCharactersAPIStore.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Foundation

// MARK: - ListCharactersAPIStore

/// ListCharactersAPIStore is a class responsible for fetching albums
final class ListCharactersAPIStore {
    
}

// MARK: - ArtistsStoreProtocol

extension ListCharactersAPIStore: ListCharacterStoreProtocol {
    /// Fetches a list of characters
    ///
    /// - parameter completion: The completion block
    func fetchCharacters(page: Int, completion: @escaping (Results<CharactersRickAndMorty>) -> Void) -> RequestToken {

        let webResource = WebResource<CharactersRickAndMorty>(urlPath: .getAllCharactersRickAndMorty(page), method: .get, header: nil, decode: (CharactersRickAndMorty.decode))
        return WebResourceManager.shared.fetchRequest(resource: webResource, completion: completion)
    }
}
