//
//  ListCharacterStoreProtocol.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright © 2018 Iniguez Ollero, Marco. All rights reserved.
//

import Foundation

// MARK: - IListCharacterStoreError

/// _ItemsStoreError_ is an enumeration for artist store errors
///
/// - generic:         Generic error
/// - invalidURL:      Invalid URL error
/// - invalidResponse: Invalid response
enum ListCharacterStoreError: Error {
    
    case generic
    case invalidURL
    case invalidResponse
}

// MARK: - ArtistsStoreProtocol

/// _ArtistsStoreProtocol_ is a protocol  for artist store behaviors
protocol ListCharacterStoreProtocol {
    
    /// Fetches artists from a store (API, memory, etc)
    ///
    /// - parameter completion: The completion block
    func fetchCharacters(page: Int, completion: @escaping (Results<CharactersRickAndMorty>) -> Void) -> RequestToken
}
