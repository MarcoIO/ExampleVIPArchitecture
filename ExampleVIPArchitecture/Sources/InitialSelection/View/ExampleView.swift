//
//  ExampleView.swift
//  ExampleVIPArchitecture
//
//  Created by marco.iniguez.ollero on 04/02/2020.
//  Copyright © 2020 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

class ExampleView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    class func instanceFromNib() -> ExampleView {
        return Bundle.main.loadNibNamed("ExampleView", owner: nil, options: nil)?[0] as! ExampleView
    }
}
