//
//  InitialSelectionViewController.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

protocol InitialSelectionViewControllerInput: InitialSelectionPresenterOutput {

}

protocol InitialSelectionViewControllerOutput {

}

final class InitialSelectionViewController: UIViewController {

    var output: InitialSelectionViewControllerOutput!

    // MARK: - Initializers

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

        configure()
    }

    // MARK: - Configurator

    private func configure(configurator: InitialSelectionConfigurator = InitialSelectionConfigurator.sharedInstance) {

        configurator.configure(viewController: self)
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {

        super.viewDidLoad()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        let viewC = ExampleView.instanceFromNib()
//        viewC.frame = self.view.frame
//        self.view.addSubview(viewC)
//        self.view.bringSubviewToFront(viewC)
    }

    // MARK: - Load data
   
}

// MARK: - InitialSelectionPresenterOutput

extension InitialSelectionViewController: InitialSelectionViewControllerInput {

    // MARK: - Display logic
    
}


#if DEBUG
import SwiftUI

@available(iOS 13, *)
struct InitialSelectionViewControllerPreview: PreviewProvider {
    
    static var previews: some View {
        // view controller using programmatic UI
        UIStoryboard(name: "InitialSelection", bundle: nil).instantiateViewController(identifier: "InitialSelectionViewController").toPreview()
    }
}
#endif
