//
//  InitialSelectionConfigurator.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

final class InitialSelectionConfigurator {

    // MARK: - Singleton

    static let sharedInstance: InitialSelectionConfigurator = InitialSelectionConfigurator()

    // MARK: - Configuration

    func configure(viewController: InitialSelectionViewController) {

        let presenter = InitialSelectionPresenter(output: viewController)
        let interactor = InitialSelectionInteractor(output: presenter)

        viewController.output = interactor
    }
}
