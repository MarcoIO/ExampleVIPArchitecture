//
//  InitialSelectionInteractor.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

protocol InitialSelectionInteractorInput: InitialSelectionViewControllerOutput {

}

protocol InitialSelectionInteractorOutput {

}

final class InitialSelectionInteractor {

    let output: InitialSelectionInteractorOutput
    let worker: InitialSelectionWorker

    // MARK: - Initializers

    init(output: InitialSelectionInteractorOutput, worker: InitialSelectionWorker = InitialSelectionWorker()) {

        self.output = output
        self.worker = worker
    }
}

// MARK: - InitialSelectionInteractorInput

extension InitialSelectionInteractor: InitialSelectionViewControllerOutput {

    // MARK: - Business logic

}
