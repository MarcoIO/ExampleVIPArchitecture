//
//  InitialSelectionPresenter.swift
//  ExampleVIPArchitecture
//
//  Created by Iniguez Ollero, Marco on 27/11/18.
//  Copyright (c) 2018 Iniguez Ollero, Marco. All rights reserved.
//

import UIKit

protocol InitialSelectionPresenterInput: InitialSelectionInteractorOutput {

}

protocol InitialSelectionPresenterOutput: class {

}

final class InitialSelectionPresenter {

    private(set) weak var output: InitialSelectionPresenterOutput!

    // MARK: - Initializers

    init(output: InitialSelectionPresenterOutput) {

        self.output = output
    }
}

// MARK: - InitialSelectionPresenterInput

extension InitialSelectionPresenter: InitialSelectionPresenterInput {

    // MARK: - Presentation logic
    
}
